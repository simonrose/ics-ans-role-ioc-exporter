def test_prometheus_ioc_exporter_is_enabled_and_running(host):
    exporter = "ioc-exporter"
    service = host.service(exporter)
    assert service.is_enabled
    assert service.is_running

    assert host.file(f"/var/log/prometheus/{exporter}.log").exists


def test_prometheus_pv_exporter_is_enabled_and_running(host):
    exporter = "pv-exporter"
    service = host.service(exporter)
    assert service.is_enabled
    assert not service.is_running

    assert host.file(f"/var/log/prometheus/{exporter}.log").exists


def test_pv_exporter_was_shutdown_by_watchdog(host):
    cmd = host.run("journalctl -u pv-exporter")
    assert cmd.rc == 0
    assert "pv-exporter.service watchdog timeout" in cmd.stdout
