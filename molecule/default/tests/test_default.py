import pytest


@pytest.mark.parametrize("exporter", ["ioc-exporter", "pv-exporter"])
def test_prometheus_exporters_are_enabled_and_running(host, exporter):
    service = host.service(exporter)
    assert service.is_enabled
    assert service.is_running

    assert host.file(f"/var/log/prometheus/{exporter}.log").exists


def test_ioc_exporter(host):
    ioc_exporter_port = "12110"
    cmd = host.command(f"curl localhost:{ioc_exporter_port}")

    if "no-iocs" in host.ansible.get_variables()["inventory_hostname"]:
        assert "ioc_dirs 0.0" in cmd.stdout
    else:
        assert "ioc_dirs 8.0" in cmd.stdout

        # Check that all metrics are there even though there were exceptions because of fake IOCs
        assert 'ioc_service_disabled{ioc="annotated-clean"} 0.0' in cmd.stdout
        assert 'ioc_essioc_missing{ioc="annotated-clean"} 1.0' in cmd.stdout
        assert 'ioc_cellmode_used{ioc="annotated-clean"} 0.0' in cmd.stdout

        # Clean
        assert 'ioc_repository_dirty{ioc="annotated-clean"} 0.0' in cmd.stdout
        assert 'ioc_repository_dirty{ioc="lightweight-clean"} 0.0' in cmd.stdout
        assert 'ioc_repository_local_commits{ioc="annotated-clean"} 0.0' in cmd.stdout
        assert 'ioc_repository_local_commits{ioc="lightweight-clean"} 0.0' in cmd.stdout

        # Dirty
        assert 'ioc_repository_dirty{ioc="annotated-dirty"} 1.0' in cmd.stdout
        assert 'ioc_repository_dirty{ioc="lightweight-dirty"} 1.0' in cmd.stdout
        assert 'ioc_repository_local_commits{ioc="annotated-dirty"} 0.0' in cmd.stdout
        assert 'ioc_repository_local_commits{ioc="lightweight-dirty"} 0.0' in cmd.stdout

        # Local commit
        assert 'ioc_repository_dirty{ioc="annotated-local-commit"} 0.0' in cmd.stdout
        assert 'ioc_repository_dirty{ioc="lightweight-local-commit"} 0.0' in cmd.stdout
        assert (
            'ioc_repository_local_commits{ioc="annotated-local-commit"} 1.0'
            in cmd.stdout
        )
        assert (
            'ioc_repository_local_commits{ioc="lightweight-local-commit"} 1.0'
            in cmd.stdout
        )


def test_logs(host):
    if "no-iocs" not in host.ansible.get_variables()["inventory_hostname"]:
        assert (
            "[Errno 2] No such file or directory: '/opt/iocs/empty/.deployment/metadata.yml"
            in host.file("/var/log/prometheus/ioc-exporter.log").content_string
        )

        assert (
            "[Errno 2] No such file or directory: '/opt/iocs/empty/.deployment/metadata.yml"
            in host.file("/var/log/prometheus/pv-exporter.log").content_string
        )

        assert (
            "[Errno 2] No such file or directory: '/opt/iocs/incorrect/ioc.json'"
            in host.file("/var/log/prometheus/ioc-exporter.log").content_string
        )

        assert (
            "Failure trying to identify git repository: /opt/iocs/incorrect"
            in host.file("/var/log/prometheus/ioc-exporter.log").content_string
        )

        assert (
            "Timeout while trying to get pvs:"
            not in host.file("/var/log/prometheus/pv-exporter.log").content_string
        )
