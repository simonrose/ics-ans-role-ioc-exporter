"""A Python2 PV monitor for Prometheus exporters.

This script creates PVA monitors to PVs
"""

import threading

from p4p.client.raw import Cancelled
from p4p.client.thread import Context

PVA_CONTEXT = Context("pva")


class PVMonitor(object):
    def __init__(self, logger):
        self.logger = logger
        self.__pvs = {}
        self.__pvs_lock = threading.Lock()
        self.__subscriptions = {}
        self.__subs_lock = threading.Lock()
        self.__subs_to_close = []

    def __pv_disconnected(self, pvname):
        # Remove from self.__pvs
        with self.__pvs_lock:
            if pvname in self.__pvs:
                self.logger.error("Exception for PV {}".format(pvname))
            else:
                # The PV starts as disconnected; return early _without_ closing the subscription
                # Future improvement: close and remove the subscription if not connected in 1 second?
                return
            self.__pvs.pop(pvname, None)

        # Close subscription and remove from self.__subscriptions
        # Future improvement: do this only if still disconnected after X seconds?
        with self.__subs_lock:
            # Cannot close the subscription from the handler ==>
            # calling .close() will result in another callback ==>
            # deadlock **but not because of __subs_lock**
            # Let's put the subscription into a list and close it later
            # Problem with this is that the PV might get connected before we close the subscription
            sub_to_close = self.__subscriptions.pop(pvname, None)
            if sub_to_close:
                self.__subs_to_close.append(sub_to_close)

    def __pv_callback(self, pvname, value):
        if isinstance(value, Cancelled):
            # Was the result of us calling .close() from subscribe_to_pvs()
            # return here without calling __pv_disconnected() to prevent trying
            # to lock __subs_lock or removing a possible new subscription to the same PV
            return

        if isinstance(value, Exception):
            self.__pv_disconnected(pvname)
            return

        # Check if PV is in subscriptions; if not then it means the subscription was put
        # into the __subs_to_close list but is not closed yet AND the PV is now connected
        # In that case we have to ignore this callback; a new subscription will be created
        with self.__subs_lock:
            if pvname not in self.__subscriptions:
                return

        with self.__pvs_lock:
            if pvname not in self.__pvs:
                self.logger.error("PV {} is now connected: {}".format(pvname, value))
            self.__pvs[pvname] = value

    def subscribe_to_pvs(self, pvs):
        with self.__subs_lock:
            # Close subscriptions to disconnected PVs
            while self.__subs_to_close:
                self.__subs_to_close.pop().close()

            for pv in pvs:

                def cb(value, pvname=pv):
                    self.__pv_callback(pvname, value)

                if pv not in self.__subscriptions:
                    self.__subscriptions[pv] = PVA_CONTEXT.monitor(
                        pv, cb, notify_disconnect=True
                    )

    def get_pv(self, pv):
        with self.__pvs_lock:
            return self.__pvs.get(pv, None)
